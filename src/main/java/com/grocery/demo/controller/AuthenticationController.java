package com.grocery.demo.controller;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AuthenticationController {

    @GetMapping("/login")
    public String showLoginForm(){
        return "groceries/login";
    }

    @GetMapping("logout")
    public String showLogoutForm(){
        return "groceries/logout";
    }
}

