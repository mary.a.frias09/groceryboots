package com.grocery.demo.model;


import javax.persistence.*;


@Entity//creating an entity to use spring's built in hibernate to connect to database and create an entity in the database
@Table(name="groceries")//creating a table in your database(mysql) called employees
public class Grocery {//just a java class

    @Id//Every entity must come with an id here we create the id
    @GeneratedValue(strategy=GenerationType.IDENTITY)//an id will automatically be generate for each employee added to the db
    private long id;//all variables created must be private we can access them through getters and setters

    @Column(name="name")//create a column in database called first_name
    private String name;

    @Column(name="category")//create a column in database called last_name
    private String category;

    @Column(name="quantity")//create a column in database called email
    private String quantity;

    public Grocery() {
    }


    //Below are the getters and setters this is how we access our variables we call these methods wherever we need to get variables or set them


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
       this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
}

